
# Static git log generator

I made this because:

- I wanted html files showing summaries and information about git projects

- Also wanted the ability to have a static site with said information for others to use and get information on projects 

- I wanted a static repository generator that I could make look much better than stagit or git-arr with only essential and or minimal javascript (also a static git repository browser that shows code signing status)


You may want to consider this tool because:

- Its fancier than stagit and git-arr to show case projects  

- You don't mind running this script via cron or git hook, rather than a dedicated service running 24/7

- If your gitlab, cgit, gitweb or other service are too slow to load under heavy traffic load (these static generators out perform these large services)

# Disclaimer

This is now in beta mode. I do want to add some other useful features.  

Its bare bones but can be used in small production enviornments. 

I haven't had the chance to see how this would hold up with thousands of git repositories. Only a few hundred.  

There are certain things I am working on. 

- eg. giving users the ability to see specific raw files

- eg. improve the code signing feature its an interesting feature I'd like to see how I can improve it (as an example a GPG key I have has expired so how do we keep track of old keys?)

- eg. making the work flow of the code as CPU efficent as possible (so far its working pretty fast thanks to cmark but it could be faster if we disable archives) 

However if you have a certain feature in mind, let me know via email, social media, and or github or gitlab.

# Requirements

Debian
````
~$ sudo apt install cmark sed bash cron
````

Fedora
````
~$ sudo dnf install cmark sed bash cron
````

# Useage

````
~$ bash git2md.sh /path/to/repositories /path/to/output 
````

and thats it. 

Just tell Apache or Nginx to serve "/path/to/output" and set a cron job or git hook to continually update git repos every few hours. Then just sit back and profit. 

Eg. with nginx you can just use this simple oneliner directive

````
location ^~ / { alias /path/to/output; try_files $uri $uri/ =404; index index.html; autoindex on;}
````


# Screenshots

![Index](https://pixelfed.sdf.org/storage/m/d6eeff88aa0c9af7fd5f87cd6431a14d929d049b/1dbfefac8530619a496d0b0ffb468e305277f351/p1QeK9tOfCvcO7xDys41xpQCgFMn5LU1F8wSRWYj.png)

![Top](https://pixelfed.sdf.org/storage/m/d6eeff88aa0c9af7fd5f87cd6431a14d929d049b/1dbfefac8530619a496d0b0ffb468e305277f351/OUXICkFWgGVnVG5Vp8F82CCUTgztpYdwcwpxP1Kx.png)

![Bottom](https://pixelfed.sdf.org/storage/m/d6eeff88aa0c9af7fd5f87cd6431a14d929d049b/1dbfefac8530619a496d0b0ffb468e305277f351/cKo61M1tHq5QmXsjz5fd6YzS10KBVr09dML6dVsY.png)

![Basic Test](https://pixelfed.sdf.org/storage/m/d6eeff88aa0c9af7fd5f87cd6431a14d929d049b/1dbfefac8530619a496d0b0ffb468e305277f351/wMSH1BSJnjjGR4DpSugFwlv91qYq9wNlKwIB8Pef.png)
 
