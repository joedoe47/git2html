#!/bin/bash
## Git2md
# Author: Jose Mendoza (Joedoe47@gmail)
# Gets a list of repos of a given folder and gets some basic information of them to be used in a static site generator 
# or to get a breif overview of what is in each of these repos in easy text format
# For use with dazzle in mind but it will work with any gitolite or bare repositories you have
# TODO: make this play nice with git hooks

#user defined variables

SITE_NAME="Git" #if you have a special site name in mind

ORGANIZATION="user" #this would be the username where your git server stores repositories. Eg. your github username

DESTINATION="${2:-/var/www/git}" #the web directory

SOURCE="${1:-/home/storage}" #default look for repos where gitolite or dazzle setup your repositories 

#server where your username can be queried for key verification or public key import
#keybase seemed the easiet and most modern looking GPG key server but you can modify to your needs
#%an = Author Name 
#%ae = Author Email
#%GK = GPG key
KEYSERVER="https://keybase.io/%an"

# add cloneable URLs
# ~$ git2md.sh /home/storage/academic /var/www/git
#
#  "git://git.joepcs.com/${SOURCE##*/}" ->   git://git.joepcs.com/academic/REPO
#  "https://gitlab.com/$ORGANIZATION" ->     https://gitlab.com/user/REPO
CLONE_URLS=(
    "https://gitlab.com/$ORGANIZATION"
); 

# branches that we want to make zip or tar.gz archive of 
BRANCHES=(
    "master"
) 

#default description for repos lacking description (git --bare automatically adds this)
DUMMY_DESCRIPTION="No description found." 

DATE="$(date +'%c' -u)" #date/time in UTC format (gives a little privacy)

#sed scripts we apply to the HTML output 
#0. HTML minify
#1. add a gravatar. optional in template.
#2. get today's date
#3. add the site url
#4. add the site name 
SCRIPTS=(
    ':a;N;$!ba;s/>\s*</></g' 
    "s|{{GRAVATAR}}|<img src='https\://www.gravatar.com/avatar/${GRAVATAR}' height='${GRAVATAR_SIZE}' width='${GRAVATAR_SIZE}'>|g" 
    "s,{{DATE}},${DATE},g"    
    "s|{{SITE_URL}}|https\://${SITE_URL}|g" 
    "s/{{SITE_NAME}}/${SITE_NAME}/g" 
    )

#HTML Template
# 0. header
# 1. menu
# 2. comments
# 3. footer 
# provide full path to template to automate builds via cronjobs or hooks
BODY=(
"${0%/*}/Template/header.html" 
"${0%/*}/Template/menu.html" 
"${0%/*}/Template/footer.html"
)

#Prepare to generate. Cache contents of BODY[@] (header, menu, comment, footer) to ram.
for FILE in "${BODY[@]}"; do
  #not valid file
  test -f "${FILE}" || { printf \\n%s\\n\\n "${FILE} does not exist. Terminating."; exit 1 ; }

  #basename
  FILENAME=${FILE##*/}

  #load to ram, apply variables, and minimize
  declare ${FILENAME%.*}="$(cat "${FILE}" | sed -e ${SCRIPTS[0]} -e "${SCRIPTS[1]}" -e "${SCRIPTS[2]}" -e "${SCRIPTS[3]}" -e "${SCRIPTS[4]}" -e "${SCRIPTS[5]}")"

#not needed once ascertained
unset FILENAME

done

# Tests

#check for needed apps
DEPENDANCY=("cmark" "sed" "git")
for PKG in ${DEPENDANCY[@]}; do
hash ${PKG} 2>/dev/null || { echo >&2 "I require ${PKG} but it's not installed. Aborting."; exit 1; } 
done


#Test destination exists
test -d "${DESTINATION}" || { printf \\n%s\\n "${DESTINATION} does not exist. creating"; mkdir -p ${DESTINATION}; }

# Main Function

#Restart Main index file
# TODO: load to variable then wait till end of loop to properly minimize index page

#Set head of table up to list out repositories we find
cat <<EOF > ${DESTINATION}/index.html
${header}${menu}
<table class="table">
<thead><tr>
<th scope="col" class="table-info">Project Repository</th> 
<th scope="col" class="table-info">Description</th> 
<th scope="col" class="table-info">Author</th> 
<th scope="col" class="table-info">Signed</th> 
<th scope="col" class="table-info">Last Commit</th> 
</tr></thead>
<tbody>
EOF

# 0. start loop in ${SOURCE}
for NAME in ${SOURCE}/*; do

# 1. Other variables
BASENAME=$(basename ${NAME})
COMMIT=$(cd ${NAME}; git --no-pager log --format="%h" -n 1) # TODO: this is a band-aid. find a real fix.
DESCRIPTION=$(cat ${NAME}/description)

# 2. if $NAME appears to be bare repository continue
if [ -d "${NAME}/refs" ]; then

 cd ${NAME}

 printf \\n%s\\n\\n "Processing: ${NAME}"

 #no description. make one. (' $ git init --bare' already does this)
 #test -f "${NAME}/description" || { printf %s\\n 'No description. Set default.' ; printf %s "${DUMMY_DESCRIPTION}" > ${NAME}/description; }

 #no archive folder. make one.
 test -d ${DESTINATION}/${BASENAME}/archives/ || { mkdir -p ${DESTINATION}/${BASENAME}/archives/ ; }

 #no raw folder. make one.
 #test -d ${DESTINATION}/${BASENAME}/raw/ || { mkdir -p ${DESTINATION}/${BASENAME}/raw/ ; }

 #add newly discovered repo folder to index
 cat <<EOF >> ${DESTINATION}/index.html
<tr> <th><a href="${BASENAME}/index.html">${BASENAME}</a></th> <td>${DESCRIPTION}</td> $(git --no-pager log --pretty=format:"<td>%an</td> <td data=\"%G?\">%G?</td> <td>%ar</td> </tr>" -1)
EOF


 # 3.  if the commit is not in folder then we probably need to generate anyways
 #avoids extra work. generate only on new commit or when user deletes archive file
  if [ ! -f "${DESTINATION}/${BASENAME}/${BASENAME}-$(git --no-pager log --format="%h" -n 1)-${BRANCHES[0]}.zip" ]; then

   #Alert
   printf %s\\n "New commit. Sending $(git --no-pager log --format="%h" -n 1) to: ${DESTINATION}/${BASENAME}";

   # 4. move old acrhives
   # move all old snapshots to 'archives'  
   #mv ${DESTINATION}/${BASENAME}/*.tar.gz ${DESTINATION}/${BASENAME}/archives ; 
   mv ${DESTINATION}/${BASENAME}/*.zip ${DESTINATION}/${BASENAME}/archives &>/dev/null ;

   #delete everything but the 10 most newest files
   ls -d -1tr ${DESTINATION}/${BASENAME}/archives/ | head -n -10 | xargs -d '\n' rm -f --

   # 5. generate new archives (zip for compability tar.gz for compression)
   #snapshots (ZIP only to save on CPU cycles)
   for branch in ${BRANCHES[@]}; do 
     git archive --format zip --output ${DESTINATION}/${BASENAME}/${BASENAME}-${COMMIT}-${branch}.zip ${branch} ;
     #git archive --format tar.gz --output ${DESTINATION}/${BASENAME}/${BASENAME}-${COMMIT}-${branch}.tar.gz ${branch} ;
   done

   #clean raw
#   rm -rf ${DESTINATION}/${BASENAME}/raw/*

   #if raw folder. clean and unzip. (unzip is faster than cloning bare repo or looping through git ls-tree. less hacky code. archive is also already there)
   #test -d ${DESTINATION}/${BASENAME}/raw && { unzip -u -o ${DESTINATION}/${BASENAME}/${BASENAME}-${COMMIT}-${BRANCHES[0]}.zip -d ${DESTINATION}/${BASENAME}/raw/ ; }


   # 7. Stage file before writing
   # TODO: re-arrange page layout for more dynamic layouts
   # TODO: re-arrange Files to display in a nicer method
declare STAGE=$(cat<<EOF
${header} ${menu}
<div id="GitData">
<div class="hero">
<h1> ${BASENAME} </h1>
<h4> ${DESCRIPTION} </h4>

<a class="btn btn-sm btn-a" href="#clone">Clone</a>
&#160;
<a class="btn btn-sm btn-a" href="#log">Log</a>
&#160;
<a class="btn btn-sm btn-a" href="#files">Files</a>
&#160;
<a class="btn btn-sm btn-a" href="#readme">Readme</a>

</div>


<div class="row">

<div class="col c4">
<a name="archives"></a>
<h3>Archives:</h3>
<p>
  <a href="${BASENAME}-${COMMIT}-${BRANCHES[0]}.zip" class="btn btn-sm btn-a">zip</a>
&#160;
  <a href="archives/" class="btn btn-sm btn-a">older archives</a>
</p>
</div>


<div class="col c4">
<a name="branches"></a>
<h3>Branches:</h3>
<p><pre>
$(git branch)
</pre></p>
</div>



<div class="col c4">
<a name="tags"></a>
<h3>Releases:</h3>
<p><pre>
$(git tag -l)
</pre></p>
</div>

</div>


<br>


<div class="hero" id="clone" class="collapse in" data-parent="#GitShow">
<h3>Clone:</h3>                                                                                                                                  
<p>
$(for URL in ${CLONE_URLS[@]}; do printf %s "<code>git clone ${URL}/${BASENAME}</code><br>"; done)
</p>
</div>

<div class="hero" id="log" class="collapse in" data-parent="#GitShow">
<h3>Log:</h3>

<table class="table">
<thead><tr>
<th>Date</th> 
<th>Author (GPG Key)</th> 
<th>Commit</th> 
<th>Comment</th>
</tr>
</thead>
<tbody>
$(git --no-pager log --date=short --pretty=format:"<tr> <th>%ad</th> <td>%an (<a href=\"${KEYSERVER}\">%GK</a>)</td> <td>%h</td>  <td>%s</td> </tr>" -10)
</tbody>
</table>
</div>



<div class="hero" id="files" class="collapse in" data-parent="#GitShow">
<h3>File Tree</h3>

<ul class="list">
$(for BLOB_NAME in $(git ls-tree -r --name-only master); do echo "<li> ${BLOB_NAME} </li>" ; done)
</ul>

</div>
</div>

<div class="hero" id="readme" class="collapse in" data-parent="#GitShow">
<h3>Readme</h3>
$(git show master:README &> /dev/null && { git show master:README | cmark ; } || { git show master:README.md | cmark ; } || { git show master:readme | cmark ; } )

</div>

<!-- content/accordian group close  -->
</div>
${comment} ${footer}
EOF
)

# Minify content
declare OUTPUT="$(printf %s "${STAGE}" | sed -e ${SCRIPTS[0]} -e "${SCRIPTS[3]}" )"

# Save all output
printf %s "${OUTPUT}" > ${DESTINATION}/${BASENAME}/index.html

unset OUTPUT
#clean up
unset BASENAME
unset COMMIT
unset DESCRIPTION

 fi #End create new repo log loop

#oops we got something that isn't a folder
else print %s\\n "${NAME} is an invalid folder.";


fi #End initial loop


# End of all Repo loops
done 

#put the last part of main index.html 

cat <<EOF >> ${DESTINATION}/index.html
</tbody>
</table>

</div> ${footer}
EOF

#no longer needed
unset header
unset menu
unset comment
unset footer
